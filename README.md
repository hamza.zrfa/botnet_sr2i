# How to Make a Test

## Step 1: Start the Slave
Start the slave by specifying the following command in a PowerShell terminal:
./botnet_slave.exe 127.0.0.1 8080

Note: In a real case, the IP should be the real IP of the server, and the same goes for the port number (generally 80).

## Step 2: Run the Master and Launch an Attack
1. Run `botnet_master.exe`
2. Add the slave `127.0.0.1:8080`
3. Launch an attack in a random IP
4. In the terminal of the slave, you should see the mention "ping IP_TARGET:PORT_TARGET" followed by "Disconnected: IP_TARGET:PORT_TARGET". This means that the master has connected to the slave to tell it to "ping this guy," and after that, it has left.
